ASCII values:
() 40/41
[] 91/93
{} 123/125
<> 60/62

~~~
'util.forth include

'left var
'right var
{ $( $[ ${ $< } !left
{ $) $] $} $> } !right

(n-n)
:opposite dup $( eq? [ #1 ] [ #2 ] choose + ;

:rollback depth swap - [ drop ] times ;

'start var
(s-cf|s-f)
:balanced-brackets?
  dup 'checking:_%s\n s:format s:put
  depth n:dec !start
  'depth @start n:label
  s:keep
  repeat
    [
      fetch
      dup #0 eq? [ ] if;
      dup c:put sp depth n:put nl
      dup @left a:contains? [
        opposite
      ] [
        depth @start n:inc eq? [
          dup 'unexpected_close:_%c s:format s:put nl
          FALSE 
        ] if;
        dup rot eq? [
          'expected_ s:put dup c:put nl
          [ @start rollback ] dip 
          FALSE
        ] -if;
        drop
      ] choose
      dup [ '/!\ s:put ] -if; 
    ] sip n:inc
    'sip dl
    over #0 eq? [ FALSE ] if;
  again
  0;
  @start rollback
  '_OK s:put nl
  TRUE
;
  

'[({(<(())[]>[[{[]{<()<>>
'[(()[<>])]({[<{<<[]>>(
'{([(<{}[<>[]}>{[]{[(<()>
'(((({<>}<{<{<>}{[]{[]{}
'[[<[([]))<([[{}[[()]]]
'[{[{({}]{}}([{[{{{}}([]
'{<[[]]>}<{[{[{[]{()[[[]
'[<(<(<(<{}))><([]([]()
'<{([([[(<>()){}]>(<<{{
'<{([{{}}[<[[[<>{}]]]>[]]
 
'[[[[[[ balanced-brackets? 'checked dl [ drop ] -if
']]]]] balanced-brackets? 'checked dl drop-pair
'<><><> balanced-brackets? 'checked dl drop-pair


[
  balanced-brackets? not [ c:put nl ] if
  'next dl
] runstack

