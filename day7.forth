16,1,2,0,4,2,7,1,2,14
~~~
#16 #1 #2 #0 #4 #2 #7 #1 #2 #14

'fh var
'size var

'day7.dat file:open-for-reading !fh !size
'num d:create #10 allot

:dl s:put ':_ s:put dump-stack nl ;
:peek-addr pop pop dup n:put push push ;
:n:show n:put nl ;
:n:peek dup n:show ;
:n:label swap s:put ':_ s:put n:show ;
:n:labelp [ dup ] dip swap n:label ;

:runfile 'day7.dat swap file:for-each-line ;
:runcsv 
  &num buffer:set
  [
    buffer:empty
    [
      @fh file:read dup [
        dup buffer:add
        $, -eq?
      ] if
    ] while
    (this_is_either_,_or_LF)
    buffer:get drop
    buffer:start s:to-number
    @fh file:tell @size lt? [
      swap dup dip
    ] [ swap call #0 ] choose
    dup
  ] while
  drop
;

:runstack 
[
  dup dip
  depth #1 gt?
] while 
drop
;

'cur var 
'insertpos var
'len var
'positions d:create @size #3 / allot
#0 !len

~~~
Takes:
  Value to be inserted
  Length of array
  Address of array

Returns:
  New length of array (len+1)

Result:
  Sorts value into correct array position
~~~
(nna-n)
:insert-sorted 
  'cur var
  'arr var
  'insertpos var
  !arr swap !cur
  dup @arr + !insertpos
  [
    @insertpos @arr gt? 
      [
        @insertpos n:dec fetch
        dup @cur lt? 
          [ 
            @insertpos store
            @insertpos n:dec !insertpos
            #1
          ]
          [ drop #0 ]
          choose
      ]
      [ #0 ]
      choose
  ] while 
  @cur @insertpos store
  n:inc
;

'totaldist var
'squaredist var
#0 !totaldist
#0 !squaredist
[ 
  dup @totaldist + !totaldist (add_to_sum)
  dup dup * @squaredist + !squaredist (add_to_sum)
  @len &positions insert-sorted !len
] runcsv

@totaldist n:put nl
@squaredist n:put nl

~~~
So, we have a sorted array in &positions
And the total of all the values on the stack

The theory is this:
Assume:
  Numbers increase left to right
  Gathering point is at x
  P(n) is the position of crab n from the left (n is 1-based)
  N(p) is the how many crabs are to the left of or at position p
  D(p) is the total of crab distances from 0 for crabs left of or at position p
  t is the total number of crabs = N(inf)
  d is the total of individual crab distances from 0 = D(inf)

So, everyone left of x will have to move x - P(n). These are crabs [1, N(p))
and everyone right of x will have to move P(n) - x. These are crabs [N(p), t]

This means the total distance moved will be:
(x - P(1)) + (x - P(2)) + ... + (P(t-1) - x) + (P(t) - x)

Using N(p), the left half of this becomes:
N(x)*x - (P(1) + P(2) + ... + P(N(x)))
And the right half becomes:
(t-N(x))*(-x) + (P(N(x)+1) + ... + P(t))

Put them back together and you get:
N(x)*x - (t-N(x))*x - (P(1) + ... + P(N(x))) + (P(N(x)+1) + ... + P(t))
x*(N(x) - t + N(x)) - D(x) + (d - D(x))
x*(2*N(x) - t) - 2*D(x) + d

This is the equation we want to minimize.

Fortunately, we have d and t, and with a
sorted list of crabs, N(x) is calculable
by iterating through the list, trying all
x values, keeping track of how many crabs
we've passed.

If there aren't any local minima, the number
will start big, get smaller, and then we can
return the last number before it starts getting
bigger again.

If there are local minima, we can just iterate
through the whole list, no big.

Let's try it!

Okay that worked but part 2 is harder :(

Part 2 means cost != distance moved, instead cost = sum-factorial = n(n+1)/2 where n = distance moved

So uhhh each part becomes (x - P(1))*(x - P(1) + 1)/2 which is...heck...
1/2*(x^2 - x*P(1) + x - x*P(1) + P(1)^2 - P(1)) ...ugh...
1/2*(x^2 - (2*x+1)*P(1) + x + P(1)^2) okay
and part two is...wait...P(n)-x = -(x-P(n))...yeah I'm silly it's just minus with t-N(x). So
1/2*(N(x)*(x^2+x) - (2*x+1)*(P(1)..P(N(x))) + (P(1)^2...P(N(x))))
and
-1/2*((t-N(x))*(x^2+x) - (2*x+1)*(P(N(x)+1)...P(t)) + (P(N(x)+1)^2...P(t)^2)
aka
1/2*((N(x)-t)*(x^2+x) + (2*x+1)*(P(N(x)+1)...P(t)) - (P(N(x)+1)^2...P(t)^2)

shit no it's not just negative the 1 is still positive so it's (P(n) - x)*(P(n)-x+1)/2 which is...eh...
1/2*(P2(n) - x*P(n) + P(n) - x * P(n) + x^2 - x)
1/2*(x^2 - (2*x-1)*P(n) - x + P2(n))
1/2*((t-N(x))*(x^2-x) - (2*x-1)*(P(N(x)+1)...P(t)) + P2(N(x)+1)...P2(t))

So, all together now:

1/2*(
  N(x)*(x^2+x) - (2*x+1)*(P(1)..P(N(x))) + (P(1)^2...P(N(x))) +
  (t-N(x))*(x^2-x) - (2*x-1)*(P(N(x)+1)...P(t)) + P2(N(x)+1)...P2(t)
)
Oh boy, together for each part...
a: N(x)*x^2 + N(x)*x + t*x^2 - t*x - N(x)*x^2 + x*N(x) = 2*x*N(x) + t(x^2-x) I guess??
b: 2*x*d + d_left - d_right
c: d2

oh wow, this got much nicer actually?? putting it back together:
1/2*(2*x*N(x) + t(x^2-x) - 2*x*d - d_left + d_right + d2)
1/2*(2*x*(N(x) - d) + t(x^2-x) - D(x) + d - D(x) + d2)
1/2*(2*x*(N(x) - d) + t(x^2-x) - 2*D(x) + d + d2) <--
1/2*(2*x*N(x) + t(x^2-x) - 2*D(x) + d(1 - 2*x) + d2)

postlude: what if I take the derivative?? Or like, a half-assed integer derivative? Hmm...
C(x) = dN(x)/x = number of crabs at a given point
which nicely means dD(x)/x = C(x)*x !

d/dx:
2*x*N(x) = 2*x*(N(x-1)+C(x)) = 2*x*N(x-1) + 2*x*C(x), d/dx = 2*x*C(x)
t(x^2-x) = t(2x-1)
2*D(x) = 2*(D(x-1) + C(x)*x), d/dx = 2*x*C(x)

d/dx = 2*x*C(x) + t(2x-1) + 2*x*C(x) = 4*x*C(x) + t(2x-1)
x^
So, when d/dx = 0 we should have our minimum, I think!
4*x*C(x) = -t(2x-1)
4*x*C(x) = t-2xt
4*x*C(x)+2xt = t
2x(2*C(x)+t) = t
x(2*C(x)+t) = t/2

Hmm. So, concretely, mins are when x*(2*C(x)+1000) = 500
500/x = 2*C(x)+1000

Hmm. Let's try the piecewise derivative of t(x^2-x) again...x^2
for integers is actually 2x - 1:
x  | x^2 | dx | (=x^2 - (x-1)^2)
---------------
-1 |  1  | -- |
 0 |  0  | -1 |
 1 |  1  |  1 |
 2 |  4  |  3 |
 3 |  9  |  5 |

So:
2*x*C(x) + t(2x-1-1) + 2*x*C(x)
This doesn't change much...

4*x*C(x) + t(2x-2)

Setting equal to 0...
4*x*C(x) = t(2-2x)
2*x*C(x) = t(1-x)
2*x*C(x) = t - tx
C(x) = t*(1-x)/2x

if C(x) = 0 then...hmm...t = tx...nope

~~~
(n-n)
:n:double #-1 shift ;
:n:halve #1 shift ;
~~~
Takes N(x), x, D(x) (see above)
Outputs x*(2*N(x) - t) - 2*D(x) without consuming the stack
You have to add d to get the actual answer, but that's a constant so no
point computing it every time
Stacks and RPN is fun!
~~~
(nnn-nnnn)
:travelcost
  push push
  dup n:double @len -
  pop [ * ] sip swap
  pop [ n:double - ] sip swap
;

~~~
Takes N(x), x, D(x) (see above) and does (UGH)
1/2*(2*x*(N(x) - d) + t(x^2-x) - 2*D(x) + d + d2)
-----------a--------------b--------c--------------
~~~
(nnnn-nnnnn)
:crabcost
  push push
  dup @totaldist - n:double
  (N(x)_a')
  pop [ swap ] sip
  (N(x)_x_a'_x)
  [ * ] sip
  (N(x)_x_a_x)
  dup dup * swap - @len * +
  (N(x)_x_a+b)
  pop [ swap ] sip
  (N(x)_x_D(x)_a+b_D(x))
  n:double -
  (N(x)_x_D(x)_a+b-c)
;

~~~
So in each iteration:
d is bottom of stack
t is @len
N(x) is the array index (NOS) + 1

And then we iterate x over the range
(arr[TOS], arr[TOS-1]]
Where arr is the positions array

For multiple crabs at the same position,
we just skip over them, iterating array
index, until the next crab is at a
different position

The below code doesn't calculate for the
very rightmost position, assuming that the
last position isn't the optimal one, which
is probably a safe bet

~~~

~~~
Takes N(x), x, D(x) calculates the travel time and increments x
~~~
'bestptc var
'bestx var
(nnn-nnn)
:nexttravel 
  travelcost
  dup @bestptc lt? [ !bestptc over !bestx ] [ drop ] choose
  [ n:inc ] dip
;

~~~
Takes an index, returns the indexed item (in our reversed list) and the
following item
~~~
(n-nn)
:gettwo
  @len swap - (idx)
  dup n:dec &positions + fetch
  [ &positions + fetch ] dip
;
 
(an-)
:dumparr
  [
    dup fetch n:put nl
    n:inc
  ] times drop
;



'next var
#9999 !bestptc
#1 (idx)
#0 (D(x))
[
  [ dup gettwo !next ]
  [ over + ]
  bi*

  (x_from_n_to_next)
  (stack__N(x)_x0_D(x))
  over @next swap - (repeat_count=@next-x)
  [ nexttravel ] times
  [ n:inc dup ] [ drop ] [ swap ] tri*
  @len lt?
] while
drop-pair
'final dl
(calculate_constants)
@bestx
@bestptc @totaldist @squaredist + + n:halve
'Best:_%n_at_%n s:format s:put

~~~
&positions buffer:set
buffer:start n:put sp
buffer:end n:put nl
