$DEBUG
OPEN "day1.dat" FOR INPUT AS #1
DIM readings(3) AS INTEGER
idx = 0
lineno = 0
prev = 0
increased = 0
total = -1
DO UNTIL EOF(1)
    INPUT #1, m
    lineno = lineno + 1
    readings(idx) = m
    IF lineno > 3 THEN
        total = sum(readings())
        PRINT total
    END IF
    IF prev > -1 AND total > prev THEN
        increased = increased + 1
    END IF
    prev = total
    lineno = lineno + 1
    idx = (idx + 1) MOD 3
LOOP
PRINT "ANS:", increased

FUNCTION sum (inarr%())
    total = 0
    FOR i = 0 TO (UBOUND(inarr%) - 1)
        total = total + inarr%(i)
    NEXT
    sum = total
END FUNCTION
